#!/usr/bin/env tdaq_python

from __future__ import print_function
import sys
import time

from tkinter import *

from ispy import IPCPartition, Summer
from ispy.Viewer import Viewer

# defaults
# update interval (in seconds)
update = 5

# parse command line
if len(sys.argv) < 2:
    print("usage: ispy.py <partition> [ <update_in_seconds> ]")
    sys.exit(1)
    
part_name = sys.argv[1]

if len(sys.argv) > 2:
    update = int(sys.argv[2])

# initialize partition
p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)

df = Viewer(p, [
    # ROS and PROS
    [
    Summer('RoIB', p, [ 'DF'], 'ROIB-1',
           [ 'Last LVL1_ID', 'Checksum errors count', 'Data sample count' ]),                    
    Summer('ROS', p, [ 'DF' ], 'ROS.ROS-.*.TriggerIn0',
           ['l2RequestsQueued', 'clearRequestsLost' ],
           ['ebRequestsQueued', 'clearRequestsQueued' ]),
    Summer('PROS', p, [ 'DF' ], 'ROS-1000.L2RH',
           [ 'LVL2Requests' , 'EBRequests', 'Cleared events', 'MissingLVL2Results', 'duplicate_L1IDs', 'ResultStoreUsed' ] ),
    # Summer('ROSE', p, [ 'DF.*' ], 'ROS-[0-9]+\.ROSE',
    #        ['LVL2Requests' ],
    #        [ 'EBRequests', 'Cleared events' ])    
    ],

    # L2SV and L2PU
    [
    Summer('L2SV', p, [ 'DF.*' ], 'L2SV-[0-9]+',
           [ 'IntervalEventRate', 'AcceptedEvents',
             'RejectedEvents', 'ForcedAccepts',
             'LVL2_events', 'AvgEventRate', 'errors' ]),
    Summer('L2PU', p, [ 'DF.*' ], 'L2PU-[0-9]+',
           [ 'LVL2IntervalRate', 'NumLVL1Results', 'NumLVL2Accepts', 'NumLVL2Rejects' ]),
    Summer('L2PU.PROC', p, [ 'DF.*' ], 'L2PU-[0-9]+\.PROC', [], ['IntervalCPU',  'RSSSize', 'TotalVirtualSize' ])        
    ],

    # DFM and SFI
    [
    Summer('DFM',  p, [ 'DF.*' ], 'DFM-[0-9]+',
           [ 'Rate of built events' , 'input queue', 'number of XOFF', 'cleared events' ]),
    Summer('Empty', p, [], '', [], []),
    Summer('DFM.PROC', p, [ 'DF.*' ], 'DFM-[0-9]+\.PROC', [], ['IntervalCPU',  'RSSSize' , 'TotalVirtualSize'])
    ],
    [
    Summer('SFI',  p, [ 'DF.*' ], 'SFI-[0-9]+',
           [ 'ActualEoERate', 'RunAverageEoERate', 'NumBusyCount' ,'NumReasks', 'NumTimeouts' ],
           ['EventPayload', 'FragmentPayload' ]),
    Summer('Empty', p, [], '', [], []),
    Summer('SFI.PROC', p, [ 'DF.*' ], 'SFI-[0-9]+\.PROC', [], ['IntervalCPU',  'RSSSize' , 'TotalVirtualSize'])        
    ],

    [
    # EFD and PT
    Summer('EFD', p,  [ 'DF.*' ], 'EFD-.*',
           ['RateIn', 'RateOut', 'EventsRcv', 'EventsSent', 'EventsInside'],
           [ 'HeapOcc' ]),
    Summer('PT', p, [ 'DF.*' ], 'PT-.*:[0-9]+',
           ['EF decisions', 'EF Accepts', 'EF Rejects'] ),
    Summer('PT.PROC', p, [ 'DF.*' ], 'PT-.*:[0-9]+.*\.PROC', [], ['IntervalCPU',  'RSSSize' , 'TotalVirtualSize'])        
    ],

    # SFO
    [
    Summer('SFO',  p, [ 'DF.*' ], 'SFO-[0-9]+',
           [ 'CurrentEventReceivedRate', 'CurrentDataReceivedRate', 'CurrentEventSavedRate', 'CurrentDataSavedRate', 'EventsReceived', 'EventsSaved' ]),
    Summer('Empty', p, [], '', [], []),
    Summer('SFO.PROC', p, [ 'DF' ], 'SFO-[0-9]+.*\.PROC', [], ['IntervalCPU',  'RSSSize' , 'TotalVirtualSize'])        
    ],
    ],
    update)

df.watch('L2SV.IntervalEventRate', lambda x,*other : x < 10000.)
df.watch('DFM.input queue', lambda x,*other : x > 900)
df.watch('DFM.Rate of built events', lambda  x,*other : x == 0)
df.watch('L2PU.NumLVL2Accepts', lambda  x,*other : x == 0)
df.watch('PT.EF Accepts', lambda  x,*other : x == 0)
df.watch('SFI.ActualEoERate', lambda  x,*other : x == 0)
df.watch('EFD.RateIn', lambda  x,*other : x == 0)
df.watch('SFO.CurrentEventReceivedRate', lambda  x,*other : x == 0)

class CheckIncrease(object):

   def __init__(self):
       self.last = 0

   def check_value(self, value, *other):
       old = self.last
       self.last = value
       return  value > old


check_forcedAccept = CheckIncrease()
check_xoff = CheckIncrease()
check_numBusyCount = CheckIncrease()
check_reasks = CheckIncrease()
check_timeouts = CheckIncrease()
            
df.watch('L2SV.ForcedAccepts', check_forcedAccept.check_value)
df.watch('DFM.number of XOFF', check_xoff.check_value)
df.watch('SFI.NumBusyCount', check_numBusyCount.check_value)
df.watch('SFI.NumReasks', check_reasks.check_value)
df.watch('SFI.NumTimeouts', check_timeouts.check_value)

df.run()
