#!/usr/bin/env tdaq_python

# Configuration

from __future__ import print_function

part_name = 'rhauser_test'

# End Configuration

import sys
print(sys.argv)
if len(sys.argv) < 3:
    print("usage: ispymonitor.py <partition> <server> [ <criteria> [ <attribute> ] ]")
    sys.exit(1)
    
part_name = sys.argv[1]
server    = sys.argv[2]

if len(sys.argv) > 3:
    criteria  = sys.argv[3]
else:
    criteria = '.*'


if len(sys.argv) > 4:
    attribute = sys.argv[4]
else:
    attribute = None

import time

from ispy import *
from ispy.MultiInfoReader import *

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)

####################################################

reader = MultiInfoReader(p, [ server ], criteria)

while True:
    reader.update()
    for i in reader.objects:
        if not attribute:
            print(reader.objects[i])
        else:
            print(i + '.' + attribute,':',getattr(reader.objects[i], attribute))
    time.sleep(5)
    
