#!/usr/bin/env tdaq_python

from __future__ import print_function

part_name = 'rhauser_test'

import sys
from ispy import *

if len(sys.argv) > 1:
   part_name = sys.argv[1]

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition:",p.name(),"is not valid")
    sys.exit(1)

l2sv = []

it = ISInfoIterator(p,'DF',ISCriteria('L2SV-[0-9]+'))
while it.next():
   if it.type().name() == 'L2SV':
      x = ISObject(p, it.name(), 'L2SV')
      l2sv.append(x)

sum = ISObject(p,'DF.L2SV-Summary','L2SV')
sum.LVL2_events = 0

for x in l2sv:
   x.checkout()
   sum.LVL2_events += x.LVL2_events
   
sum.checkin(False)

print(sum)


