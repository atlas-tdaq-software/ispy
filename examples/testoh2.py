#!/usr/bin/env tdaq_python
from oh import *
from ipc import IPCPartition

import ROOT

# why do I need this ??
ROOT.TObject

part = 'rhauser_test'
import sys
if len(sys.argv) > 1:
    part = sys.argv[1]

p = IPCPartition(part)
h = getRootObject(p,'Histogramming', 'L2PU-8191','/DEBUG/LVL2PUHistograms/DataPerAcceptedEvent')
h.Draw()
prov = OHRootProvider(p,'Histogramming','MyTest', None)
prov.publish(h, 'MyHisto')
prov.publish(h, 'MyHisto')
