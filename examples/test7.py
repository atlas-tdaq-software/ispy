#!/usr/bin/env tdaq_python

from __future__ import print_function

import sys
import time
from ispy import *

p = IPCPartition(sys.argv[1])
m = Merger(p, 'DF.*', 'L2SV-[0-9]+', 'L2SV', ['IntervalTime'], 'DF.L2SV-Summary')
while True:
    m.update()
    print(m.result)
    time.sleep(5)

