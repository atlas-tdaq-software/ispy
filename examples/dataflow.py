#!/usr/bin/env tdaq_python

from __future__ import print_function

import sys
import time

from ispy import *
from ispy.Summer import *

# defaults
# update interval (in seconds)
update = 5

# parse command line
if len(sys.argv) < 2:
    print("usage: dataflow.py <partition> [ <update_in_seconds> ]")
    sys.exit(1)
    
part_name = sys.argv[1]

if len(sys.argv) > 2:
    update = int(sys.argv[2])

# initialize partition
p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)

class Dataflow(object):

    def __init__(self, partition, readers):
        self.partition = partition
        self.readers   = readers


    def add_reader(self, reader):
        self.readers.append(reader)
                                                                       
    def run(self):
        while True:
            print()
            print("########### " + time.ctime() + " ###########")
            print("=============================================")            
            for reader in self.readers:
                reader.update();
                reader.dump()
                print("=============================================")

            time.sleep(update)
    
df = Dataflow(p, [
    Summer('ROS', p, [ 'DF' ], 'ROS.ROS-[0-9]+.TriggerIn0', ['l2RequestsQueued', 'ebRequestsQueued',
                                                         'clearRequestsQueued', 'clearRequestsLost' ]),
    Summer('L2SV', p, [ 'DF.*' ], 'L2SV-[0-9]+', [ 'IntervalEventRate', 'LVL1_events', 'LVL2_events', 'AcceptedEvents', 
                                                   'RejectedEvents', 'ForcedAccepts', 'AvgEventRate', 'errors' ]),
    Summer('L2PU', p, [ 'DF.*' ], 'L2PU-[0-9]+', [ 'LVL2IntervalRate', 'NumLVL1Results', 'NumLVL2Accepts', 'NumLVL2Rejects' ]),
    Summer('DFM',  p, [ 'DF.*' ], 'DFM-[0-9]+',  [ 'Rate of built events' , 'input queue', 'number of XOFF', 'cleared events' ]),
    Summer('SFI',  p, [ 'DF.*' ], 'SFI-[0-9]+',  [ 'ActualEoERate', 'RunAverageEoERate', 'NumBusyCount' ], ['EventPayload', 'FragmentPayload' ]),
    Summer('SFO',  p, [ 'DF.*' ], 'SFO-[0-9]+',  [ 'CurrentEventReceivedRate', 'CurrentDataReceivedRate' ])
    ]
)
df.run()

