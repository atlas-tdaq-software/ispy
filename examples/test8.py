#!/usr/bin/env tdaq_python

from __future__ import print_function

from ipc import IPCPartition
from ispy import *

import sys

p = IPCPartition(sys.argv[1])
x = ISObject(p,'RunCtrl.SFI-1','RCStateInfo')
x.checkout()
print(x)
print(x.toXML())

