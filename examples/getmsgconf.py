#!/usr/bin/env tdaq_python
#
# usage: getmsgconf.py [ partition [ hosts ] ]
#
# e.g.:
#    getmsgconf.py rhauser_test L2SV-.*
#

# partition name
from __future__ import print_function
part_name = 'rhauser_test'

# all hosts
hosts = '.*'

import sys
from ispy import IPCPartition, InfoReader


def pretty_print(node):
    print("%10s" % node.Name, node.Hostname, node.NodeID, node.Interfaces, node.Port)

if len(sys.argv) > 1:
    part_name = sys.argv[1]

if len(sys.argv) > 2:
    hosts = sys.argv[2]

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition:",p.name(),"is not valid")
    sys.exit(1)

info = InfoReader(p, 'DF', hosts + '\.MSGCONF')
info.update()

for node in info.objects:
    pretty_print(info.objects[node])

