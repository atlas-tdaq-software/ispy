
from ispy import ISInfoIterator, ISInfoDynAny, ISCriteria

class InfoReader(object):
    """
    A helper class to read a subset of IS information
    and create the corresponding python objects.
    After an update() call the 'objects' attribute
    will contain all ISInfo objects that matched
    the regular expression passed in the constructor
    (defaul is '.*', i.e. all objects).
    """

    def __init__(self, partition, server, selection = '.*'):
        self.partition = partition
        self.server    = server
        self.criteria  = ISCriteria(selection)
        self.objects = {}

    def update(self):
        "Update objects from IS server"
        it = ISInfoIterator(self.partition, self.server, self.criteria)

        self.objects = {}
        while it.next():
            any = ISInfoDynAny()            
            it.value(any)
            self.objects[it.name()] = any

