#!/usr/bin/env tdaq_python

from libispy import *

# for backwards compat
from ipc import *

#
# The following methods modify the ISInfoAny
# class inside python. They create dynamically
# any attributes that the original IS object
# has.
#
# This code assumes that there is a 'ISInfoDocument'
# for the class available.
#

def _update_any(self, name, partition):
    self._name = name
    self._doc = ISInfoDocument(partition, self.type())
    self._descr = {}

    for i in range(self._doc.attributeCount()):
        attr = self._doc.attribute(i)
        setattr(self, attr.name(), self.get())
        self._descr[attr.name()] = attr.description()

        
def _get_name_any(self):
    return self._name

def _str_any(self):
    result = 'NAME : ' + self._name + '\n'
    result += 'TYPE : ' + self.type().name() + '\n'
    result += 'TIME : ' + self.time().str() + '\n'
    for a in dir(self):
        if a.startswith('_'): continue
        x = getattr(self, a)
        if callable(x): continue
        result += '%s : ' % a
        result += str(getattr(self, a)) + '\n'
    return result

ISInfoAny._update      = _update_any
ISInfoAny.get_name     = _get_name_any
ISInfoAny.__str__      = _str_any

# helper methods for OWLtime
OWLTime.__str__ = OWLTime.str
OWLTime.__repr__ = lambda self: '<' + self.__class__.__module__ + '.' + self.__class__.__name__ + ' ' + self.str() + '>'
OWLDate.__str__ = OWLDate.str

# helpers for ISType
ISType.__repr__ = lambda self: '<ISType ' + self.name() + '>'

# helpers for ISServerIterator
def _server_iterator(it):
    it.reset()
    while it.next():
        yield (it.name(), it.owner(), it.host(), it.pid(), it.time())

ISServerIterator.__iter__ = lambda self: _server_iterator(self)

# helpers for ISInfoIterator
def _info_iterator(it):
    it.reset()
    while it.next():
        yield (it.name(), it.type(), it.time(), it.tag())

ISInfoIterator.__iter__ = lambda self: _info_iterator(self)

def _dynany_str(self):
    result = ''
    for i in range(0, self.getAttributesNumber()):
        a = self.getAttributeDescription(i)
        result += a.name() + ' : ' + a.typeName() + ' = ' + str(self.getAttributeValue(i)) + '\n'
    return result

def _dynany_get(self, name):
    try:
        return self.getAttributeValue(name)
    except:
        raise AttributeError(name)

def _dynany_set(self, name, value):
    if name.startswith('_'):
        self.__dict__[name] = value
    else:
        self.setAttributeValue(name, value)

def _dynany_setdict(self, *args, **kw):
    if args:
        for k in list(args[0].keys()):
            self[k] = args[0][k]
    for k in list(kw.keys()):
        self[k] = kw[k]

ISInfoDynAny.set = _dynany_setdict

ISInfoDynAny.__str__     = _dynany_str
ISInfoDynAny.__getattr__ = _dynany_get
ISInfoDynAny.__setattr__ = _dynany_set
ISInfoDynAny.__getitem__ = _dynany_get
ISInfoDynAny.__setitem__ = _dynany_set
ISInfoDynAny.__dir__     = lambda self: ['time', 'type' ] + [ self.getAttributeDescription(i).name() for i in range(self.getAttributesNumber()) ]
ISInfoDynAny.keys = lambda self: [ self.getAttributeDescription(i).name() for i in range(self.getAttributesNumber()) ]
ISInfoDynAny.__len__     = lambda self: self.getAttributesNumber()

def _iter(obj):
   for index in range(len(obj)): yield obj[index]

ISInfoDynAny.__iter__    = lambda self: _iter(self)
ISInfoDynAny.__repr__    = lambda self: '<IS.' + self.type().name() + ' at ' + hex(id(self)) + '>'
ISInfoDynAny.__contains__ = lambda self, key: key in list(self.keys())

def _dict_getitem(self, key):
    ret_ = ISInfoDynAny()
    self.getValue(key, ret_)
    return ret_

def _dict_setitem(self, key, value):
    self.checkin(key, value)

ISInfoDictionary.__getitem__ = _dict_getitem
ISInfoDictionary.__setitem__ = _dict_setitem

# helper for InfoStream
def _infostream_next(self):
    while not self.eof():
        (name_, type_, value_) = (self.name(), self.type(), ISInfoDynAny())
        self.get(value_)
        yield (name_, type_, value_)

ISInfoStream.__iter__ = lambda self: _infostream_next(self)

# for backward compatibility
def _inforeceiver_subscribe(self, server, handler, criteria):
    def _handler(cb):
        v = ISInfoDynAny()
        cb.value(v)
        handler(v, cb.name())
        
    self.subscribe_info(server, _handler, criteria)
    
ISInfoReceiver.subscribe = _inforeceiver_subscribe

class ISObject(ISInfoDynAny):

    def __init__(self, partition, obj_name, type_name = None, *args, **kw):
        if type(partition) is str:
            partition = IPCPartition(partition)
        self._d       = ISInfoDictionary(partition)
        self._name    = obj_name
        if type_name:
            ISInfoDynAny.__init__(self, partition, type_name)
        else:
            ISInfoDynAny.__init__(self)

        if args:
            for arg in args:
                for i in list(arg.keys()):
                    self[i] = arg[i]
                
        for i in list(kw.keys()):
            self[i] = kw[i]

    def __str__(self):
        return 'NAME: ' + self._name + '\n' + super(ISObject, self).__str__()

    def __repr__(self):
        return '<IS.' + self.type().name() + ' ' + self._name + '>'
            
    def name(self):
        return self._name

    def checkout(self, tag = None):
        if tag:
            self._d.getValue(self._name, tag, self)            
        else:
            self._d.getValue(self._name, self)

    def checkin(self, hist_or_tag = False):
        if type(hist_or_tag) is bool:
            self._d.checkin(self._name, self, hist_or_tag)
        else:
            self._d.checkin(self._name, hist_or_tag, self)

    def remove(self):
        self._d.remove(self._name)

    def exists(self):
        return self._d.contains(self._name)

    def isExist(self):
        return self.exists()

    def tags(self):
        return self._d.getTags(self._name)

import types
class _M(types.ModuleType):

    def __getattr__(self, key):

        if key.startswith('HistogramData'):
            key = 'HistogramData<' + key[13:].lower() + '>'

        if key.startswith('Vector'):
            key = 'Vector<' + key[6:] + '>'
            
        if key not in self.__dict__:        

            def _init(self, partition_, name_, **kw):
                ISObject.__init__(self,partition_, name_, key, **kw)
        
            self.__dict__[key] = type(key, (ISObject,), { '__init__' : _init })
            
        return self.__dict__[key]

    def types(self,partition):
        import ispy
        import config
        c = config.Configuration('rdbconfig:ISRepository@' + partition.name())
        return c.classes()

IS = _M('IS')

from ispy.Merger import Merger
from ispy.InfoReader import InfoReader
from ispy.MultiInfoReader import MultiInfoReader
from ispy.Summer import Summer
# from ispy.Viewer import Viewer
