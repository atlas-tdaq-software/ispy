from __future__ import absolute_import

from tkinter import *

from ispy import ISInfoIterator, ISInfoAny, ISCriteria
from . import ToolTip
import config

import time

def flatten(lst):
    x = []
    for i in lst:
        if isinstance(i, list):
            x.extend(flatten(i))
        else:
            x.append(i)
    return x

class StatusBar(Frame):

    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, bd=1, relief=SUNKEN, anchor=W)
        self.label.grid()

    def set(self, format, *args):
        self.label.config(text=format % args)
        self.label.update_idletasks()

    def clear(self):
        self.label.config(text="")
        self.label.update_idletasks()


class Viewer(object):

    def __init__(self, partition, readers, update_interval = 5):
        """
        Create a Tcl/Tk based viewer for a set of Summer objects.
        
        'partition' - the IPCPartition() object to use.
        'readers'   - a list of 'Summer' objects. An entry in the list
        can be another list, in which case the entries will be displayed
        vertically above each other, rather than horizontally (the default).
        'update_interval' - in seconds.
        
        """

        self.color_ok = 'black'
        self.color_notok = 'red'

        self.partition = partition
        self.readers = flatten(readers)
        self.update_interval  = update_interval
        self.update_slow_interval = 50

        self.watchers = {}

        self.runnumber = 0
        self.starttime = 'unknown'
        self.masterkey = 0
        try:
            db = config.Configuration('rdbconfig:' + self.partition.name() + '::RDB')
            part = db.get_obj('Partition', self.partition.name())
            trigconf = part.get_obj('TriggerConfiguration')
            trigdb = trigconf.get_obj('TriggerDBConnection')
            if trigdb:
                self.masterkey = db.get_dal('TriggerDBConnection',trigdb.UID()).SuperMasterKey
        except:
            pass
        

        self.tk = Tk()
        self.tk.title("Ispy: " + self.partition.name())

        self.menu = Menu(self.tk)
        
        file_menu = Menu(self.menu, tearoff=0)
        file_menu.add_command(label='Quit', command=self.tk.quit)
        self.menu.add_cascade(label='File', menu=file_menu)
        
        self.tk.config(menu=self.menu)

        # widget frames
        self.frames = []

        # Tcl/Tk variables
        self.variables = {}

        # description set ?
        self.descriptions = {}

        col = 0
        for rdr in readers:

            if isinstance(rdr, list):
                lst = rdr
            else:
                lst = [ rdr ]

            orow = 0
            for r in lst:
                self.variables[r.prefix] = {}
                self.descriptions[r.prefix] = {}
                
                frame = LabelFrame(self.tk, text=r.prefix)
                self.frames.append(frame)
                
                row = 0
                for a in r.attributes + r.averages:
                    l = Label(frame, text=a+': ')
                    l.grid(row=row, column=0, sticky=W)
                    self.descriptions[r.prefix][a] = l

                    self.variables[r.prefix][a] = StringVar()                    
                    l = Label(frame,textvariable=self.variables[r.prefix][a])
                    l.grid(row=row, column=1, sticky=E)
                    row += 1

                frame.grid(column=col,row=orow,padx=5, pady=5, sticky=N)
                orow += 1
            col += 1


        self.status = StatusBar(self.tk)
        self.status.grid(column=0,columnspan=col)
        self.tk.after(100, self.update)

        
    def get_runparams(self):
        """
        Update information from the run parameter IS info:
        - run number
        - start time
        """
        try:
            it = ISInfoIterator(self.partition, 'RunParams', ISCriteria('RunParams'))
            if it.next():
                any = ISInfoAny()
                it.value(any)
                self.runnumber = any.get()
                any.get()
                any.get()
                any.get()
                any.get()
                any.get()
                any.get()
                any.get()
                any.get()
                any.get()
                self.starttime = any.get()
        except:
            pass

    def set_color(self, prefix, attr, color):
        for f in self.frames:
            if f.cget('text') == prefix:
                for l in list(f.children.values()):
                    if l.cget('text') == attr + ': ':
                        l.config(fg=color)
                        return

    def slow_update(self):
        """
        Reset the lists of actual IS servers. This is necessary if segments
        are enabled or disabled in a running partition without restarting
        ispygui.py.
        """
        for r in self.readers:
            r.update_servers()
        self.update_slow_interval = 50
        

    def update(self):
        """
        Update all IS readers. Format values and store them
        in the appropriate tk variables. 
        """
        
        if self.partition.isValid():
          self.update_slow_interval -= 1
          if self.update_slow_interval <= 0:
              self.slow_update()

          fr = 0
          for reader in self.readers:
            reader.update()
            self.frames[fr].configure(text=reader.prefix + ' (' + str(len(reader.objects)) + ')')
            fr += 1

            for a in reader.attributes + reader.averages:

                if self.descriptions[reader.prefix].get(a) != None and len(reader.objects) > 0:
                    x = ToolTip.ToolTip(self.descriptions[reader.prefix][a],reader.objects[list(reader.objects.keys())[0]]._descr[a])
                    self.descriptions[reader.prefix][a] = None
                
                x = getattr(reader,a)
                if isinstance(x,float):
                    self.variables[reader.prefix][a].set("%.02f" % x)
                else:
                    self.variables[reader.prefix][a].set(str(x))

                if reader.prefix+'.'+a in self.watchers:
                    if self.watchers[reader.prefix+'.'+a](x, reader.prefix, a):
                        color = self.color_notok
                    else:
                        color = self.color_ok 
                    self.set_color(reader.prefix, a, color)


          # get run parameter and update status bar
          self.get_runparams()
          self.status.set("Run: %d Start: %s Trigger: %d Last update: %s" % (self.runnumber, self.starttime, self.masterkey, time.ctime()))
          self.tk.after(self.update_interval * 1000, self.update)
        else:
          self.status.set("No active partition")
          self.tk.after(30000, self.update)

    def watch(self, attribute_name, watcher):
        self.watchers[attribute_name] = watcher

    def run(self):
        self.tk.mainloop()

