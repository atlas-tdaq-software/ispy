# ISPY

## tdaq-09-05-00

### New Methods on IPCPartion

These allow to retrieve the available CORBA interfaces (types) and objects of a given type, as well as checking
if a given object is valid.

    p = IPCPartition("...")
    p.getTypes()
    ['mts/worker', 'rc/commander', 'rdb/writer', 'is/repository', 'rmgr/ResMgr', 'rdb/cursor', 'pmgpriv/SERVER', 'ipc/servant']

    p.getObjects('rdb/writer')
    {'RDB_RW': {'valid': True, 'name': 'RDB_RW_INITIAL', 'owner': 'rhauser', 'host': 'rhauser-inspiron', 'pid': 1947, 'time': 1664623815, 'debug': 0, 'verbose': 0}}

    p.isObjectValid('RDB_RW', 'rdb/writer')
    True

## IS Attribute `format()` Method

This returns a formatter that can be called to get the desired string
representation for the attribute (typically used for hex representation).

    fmt = attr.format()
    print(fmt.format(value))


## Minimal MTS Receiver Interface

To create a MTS subcription and register a callback:

    def callback(issue):
       print(issue)

    import mts

    sub = mts.add_receiver(callback, "initial", "*")

The last argument is an MTS subscription expression. The `sub` return value
should be kept alive as the subscription will be removed if it is garbage
collected. It can also be used to unsubscribe explicitly.

    sub.remove_receiver()

The `issue` passed to the callback is a Python dictionary with all
the content from an `ers::Issue`.
