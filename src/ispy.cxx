
#include "ipc/core.h"
#include "ipc/partition.h"
#include "is/type.h"
#include "is/istream.h"
#include "is/ostream.h"
#include "is/info.h"
#include "is/infoany.h"
#include "is/infodynany.h"
#include "is/namedinfo.h"
#include "is/infoiterator.h"
#include "is/infodictionary.h"
#include "is/infodocument.h"
#include "is/inforeceiver.h"
#include "is/serveriterator.h"
#include "is/criteria.h"
#include "is/infostream.h"
#include "is/callbackevent.h"
#include "is/callbackinfo.h"

#include "ers/Issue.h"

#include <boost/python.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <string>

using namespace boost::python;

namespace {

    struct callback_helper {
        callback_helper(const PyGILState_STATE& state)
            : m_state(state)
        {}
        ~callback_helper()
        {
            PyGILState_Release(m_state);
        }
        PyGILState_STATE m_state;
    };

    template<class T>
    struct ISCallbackWrapper
    {
        ISCallbackWrapper(object callback)
            : _callback(callback)
        {}

        void operator()(T* info)
        {
            // ISInfoDynAny *any = new ISInfoDynAny;
            // info->value(*any);

            callback_helper helper(PyGILState_Ensure());
            _callback(object(ptr(info)));
        }

        object _callback;
    };

    template<typename INFO>
    void subscribe_1(ISInfoReceiver& rcv,
                     const std::string& server_name,
                     object callback,
                     const ISCriteria& criteria)
    {
        rcv.subscribe(server_name, criteria, ISCallbackWrapper<INFO>(callback));
    }

    template<typename INFO>
    void subscribe_2(ISInfoReceiver& rcv,
                     const std::string& name,
                     object callback)
    {
        rcv.subscribe(name, ISCallbackWrapper<INFO>(callback));
    }

    template<typename INFO>
    void subscribe_3(ISInfoReceiver& rcv,
                     const std::string& name,
                     object callback,
                     const ISCriteria& criteria,
                     list event_mask)
    {
        switch(len(event_mask)) {
        case 0:
            rcv.subscribe(name, ISCallbackWrapper<INFO>(callback));
            break;
        case 1:
            rcv.subscribe(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]) });
            break;
        case 2:
            rcv.subscribe(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]) });
            break;
        case 3:
            rcv.subscribe(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]), extract<ISInfo::Reason>(event_mask[2]) });
            break;
        default:
            // four or more
            rcv.subscribe(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]),  extract<ISInfo::Reason>(event_mask[2]), extract<ISInfo::Reason>(event_mask[3]) });
            break;
        }
    }

    template<typename INFO>
    void subscribe_4(ISInfoReceiver& rcv,
                     const std::string& name,
                     object callback,
                     list event_mask)
    {
        switch(len(event_mask)) {
        case 0:
            rcv.subscribe(name, ISCallbackWrapper<INFO>(callback));
            break;
        case 1:
            rcv.subscribe(name, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]) });
            break;
        case 2:
            rcv.subscribe(name, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]) });
            break;
        case 3:
            rcv.subscribe(name, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]), extract<ISInfo::Reason>(event_mask[2]) });
            break;
        default:
            // four or more
            rcv.subscribe(name, ISCallbackWrapper<INFO>(callback), nullptr,
                          { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]),  extract<ISInfo::Reason>(event_mask[2]), extract<ISInfo::Reason>(event_mask[3]) });
            break;
        }
    }


    template<typename INFO>
    void scheduleSubscription_1(ISInfoReceiver& rcv,
                                const std::string& server_name,
                                object callback,
                                const ISCriteria& criteria)
    {
        rcv.scheduleSubscription(server_name, criteria, ISCallbackWrapper<INFO>(callback));
    }

    template<typename INFO>
    void scheduleSubscription_2(ISInfoReceiver& rcv,
                                const std::string& name,
                                object callback)
    {
        rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback));
    }

    template<typename INFO>
    void scheduleSubscription_3(ISInfoReceiver& rcv,
                                const std::string& name,
                                object callback,
                                const ISCriteria& criteria,
                                list event_mask)
    {
        switch(len(event_mask)) {
        case 0:
            rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback));
            break;
        case 1:
            rcv.scheduleSubscription(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]) });
            break;
        case 2:
            rcv.scheduleSubscription(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]) });
            break;
        case 3:
            rcv.scheduleSubscription(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]), extract<ISInfo::Reason>(event_mask[2]) });
            break;
        default:
            // four or more
            rcv.scheduleSubscription(name, criteria, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]),  extract<ISInfo::Reason>(event_mask[2]), extract<ISInfo::Reason>(event_mask[3]) });
            break;
        }
    }

    template<typename INFO>
    void scheduleSubscription_4(ISInfoReceiver& rcv,
                                const std::string& name,
                                object callback,
                                list event_mask)
    {
        switch(len(event_mask)) {
        case 0:
            rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback));
            break;
        case 1:
            rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]) });
            break;
        case 2:
            rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]) });
            break;
        case 3:
            rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]), extract<ISInfo::Reason>(event_mask[2]) });
            break;
        default:
            // four or more
            rcv.scheduleSubscription(name, ISCallbackWrapper<INFO>(callback), nullptr,
                                     { extract<ISInfo::Reason>(event_mask[0]), extract<ISInfo::Reason>(event_mask[1]),  extract<ISInfo::Reason>(event_mask[2]), extract<ISInfo::Reason>(event_mask[3]) });
            break;
        }
    }

    template<class T>
    object get_value(ISInfoAny& any, bool is_array)
    {
        if(is_array) {
            std::vector<T> result;
            any >> result;
            list py_result;
            for(typename std::vector<T>::const_iterator it = result.begin();
                it != result.end();
                ++it) {
                py_result.append(object(*it));
            }
            return py_result;
        } else {
            T result;
            any >> result;
            return object(result);
        }
    }

    object any_get(ISInfoAny& any)
    {
        bool   is_array  = any.isAttributeArray();
        switch (any.getAttributeType()) {

        case ISType::Boolean : return get_value<bool>(any, is_array); break;
        case ISType::S8	     : return get_value<char>(any,  is_array); break;
        case ISType::U8	     : return get_value<unsigned char>(any,  is_array); break;
        case ISType::S16     : return get_value<short>( any,  is_array); break;
        case ISType::U16     : return get_value<unsigned short>( any,  is_array); break;
        case ISType::S32     : return get_value<int>(any,  is_array); break;
        case ISType::U32     : return get_value<unsigned int>(any,  is_array); break;
        case ISType::S64     : return get_value<int64_t>(any,  is_array); break;
        case ISType::U64     : return get_value<uint64_t>(any,  is_array); break;
        case ISType::Float   : return get_value<float>(any,  is_array); break;
        case ISType::Double  : return get_value<double>(any,  is_array); break;
        case ISType::String  : return get_value<std::string>(any, is_array); break;
        case ISType::Date    : return get_value<OWLDate>(any, is_array); break;
        case ISType::Time    : return get_value<OWLTime>(any, is_array); break;
        case ISType::InfoObject  :
            {
                size_t size = 1;
                if(is_array) {
                    size = any.readArraySize();
                }

                for(size_t i = 0; i < size; i++) {

                }
            }

            // return get_value<ISInfoAny>(any, is_array); break;

        default:
            assert(false);

            break;
        }

        return object();
    }

    template<class T>
    object wrap_vector(const std::vector<T>& input)
    {
        list result;
        for(typename std::vector<T>::const_iterator it = input.begin();
            it != input.end();
            ++it) {
            result.append(object(*it));
        }
        return result;
    }

    // ISInfoDynAny
    object getAttributeValue_by_position(ISInfoDynAny& obj, size_t pos)
    {

        if(obj.isAttributeArray(pos)) {
            switch (obj.getAttributeType(pos)) {
            case ISType::Boolean : return wrap_vector(obj.getAttributeValue<std::vector<bool> >(pos));
            case ISType::S8      : return wrap_vector(obj.getAttributeValue<std::vector<char> >(pos));
            case ISType::U8	 : return wrap_vector(obj.getAttributeValue<std::vector<unsigned char> >(pos));
            case ISType::S16     : return wrap_vector(obj.getAttributeValue<std::vector<short> >(pos));
            case ISType::U16     : return wrap_vector(obj.getAttributeValue<std::vector<unsigned short> >(pos));
            case ISType::S32     : return wrap_vector(obj.getAttributeValue<std::vector<int> >(pos));
            case ISType::U32     : return wrap_vector(obj.getAttributeValue<std::vector<unsigned int> >(pos));
            case ISType::S64     : return wrap_vector(obj.getAttributeValue<std::vector<int64_t> >(pos));
            case ISType::U64     : return wrap_vector(obj.getAttributeValue<std::vector<uint64_t> >(pos));
            case ISType::Float   : return wrap_vector(obj.getAttributeValue<std::vector<float> >(pos));
            case ISType::Double  : return wrap_vector(obj.getAttributeValue<std::vector<double> >(pos));
            case ISType::String  : return wrap_vector(obj.getAttributeValue<std::vector<std::string> >(pos));
            case ISType::Date    : return wrap_vector(obj.getAttributeValue<std::vector<OWLDate> >(pos));
            case ISType::Time    : return wrap_vector(obj.getAttributeValue<std::vector<OWLTime> >(pos));
            case ISType::InfoObject  : return object(ptr(&obj.getAttributeValue<std::vector<ISInfoDynAny> >(pos)));
            default:
                assert(false);
                break;
            }
        } else {

            switch (obj.getAttributeType(pos)) {
            case ISType::Boolean : return object(obj.getAttributeValue<bool>(pos));
            case ISType::S8	 : return object(obj.getAttributeValue<char>(pos));
            case ISType::U8	 : return object(obj.getAttributeValue<unsigned char>(pos));
            case ISType::S16     : return object(obj.getAttributeValue<short>(pos));
            case ISType::U16     : return object(obj.getAttributeValue<unsigned short>(pos));
            case ISType::S32     : return object(obj.getAttributeValue<int>(pos));
            case ISType::U32     : return object(obj.getAttributeValue<unsigned int>(pos));
            case ISType::S64     : return object(obj.getAttributeValue<int64_t>(pos));
            case ISType::U64     : return object(obj.getAttributeValue<uint64_t>(pos));
            case ISType::Float   : return object(obj.getAttributeValue<float>(pos));
            case ISType::Double  : return object(obj.getAttributeValue<double>(pos));
            case ISType::String  : return object(obj.getAttributeValue<std::string>(pos));
            case ISType::Date    : return object(obj.getAttributeValue<OWLDate>(pos));
            case ISType::Time    : return object(obj.getAttributeValue<OWLTime>(pos));
            case ISType::InfoObject  : return object(ptr(&obj.getAttributeValue<ISInfoDynAny>(pos)));
            default:
                assert(false);
                break;
            }
        }

        return object();
    }

    object getAttributeValue_by_name(ISInfoDynAny& obj, const std::string& name)
    {
        return getAttributeValue_by_position(obj, obj.getDescription().attributePosition(name));
    }

    template<class T>
    std::vector<T> unwrap_vector(object obj)
    {
        std::vector<T> result;
        result.reserve(len(obj));
        for(int i = 0; i < len(obj); i++) {
            result.push_back(extract<T>(obj[i]));
        }
        return result;
    }

    void setAttributeValue_by_position(ISInfoDynAny& obj, size_t pos, object value)
    {

        if(obj.isAttributeArray(pos)) {
            switch (obj.getAttributeType(pos)) {
            case ISType::Boolean : obj.getAttributeValue<std::vector<bool> >(pos) = unwrap_vector<bool>(value); break;
            case ISType::S8	 : obj.getAttributeValue<std::vector<char> >(pos) = unwrap_vector<char>(value); break;
            case ISType::U8	 : obj.getAttributeValue<std::vector<unsigned char> >(pos) = unwrap_vector<unsigned char>(value); break;
            case ISType::S16     : obj.getAttributeValue<std::vector<short> >(pos) = unwrap_vector<short>(value); break;
            case ISType::U16     : obj.getAttributeValue<std::vector<unsigned short> >(pos) = unwrap_vector<unsigned short>(value); break;
            case ISType::S32     : obj.getAttributeValue<std::vector<int> >(pos) = unwrap_vector<int>(value); break;
            case ISType::U32     : obj.getAttributeValue<std::vector<unsigned int> >(pos) = unwrap_vector<unsigned int>(value); break;
            case ISType::S64     : obj.getAttributeValue<std::vector<int64_t> >(pos) = unwrap_vector<int64_t>(value); break;
            case ISType::U64     : obj.getAttributeValue<std::vector<uint64_t> >(pos) = unwrap_vector<uint64_t>(value); break;
            case ISType::Float   : obj.getAttributeValue<std::vector<float> >(pos) = unwrap_vector<float>(value); break;
            case ISType::Double  : obj.getAttributeValue<std::vector<double> >(pos) = unwrap_vector<double>(value); break;
            case ISType::String  : obj.getAttributeValue<std::vector<std::string> >(pos) = unwrap_vector<std::string>(value); break;
            case ISType::Date    : obj.getAttributeValue<std::vector<OWLDate> >(pos) = unwrap_vector<OWLDate>(value); break;
            case ISType::Time    : obj.getAttributeValue<std::vector<OWLTime> >(pos) = unwrap_vector<OWLTime>(value); break;
            case ISType::InfoObject  :
                {
#if 0
                    if(obj.isAttributeArray(pos)) {
                        const std::string& type_name = obj.getAttributeDescription(pos).typeName();
                        for(int i = 0; i < len(value); i++) {
                            ISInfo& info = extract<ISInfo&>(value[i]);
                            if(type_name != info.type().name()) {
                                throw daq::is::InfoNotCompatible(ERS_HERE, type_name);
                            }
                        }
                    }
#endif
                    break;
                }
            default:
                assert(false);
                break;
            }
        } else {

            switch (obj.getAttributeType(pos)) {
            case ISType::Boolean : obj.getAttributeValue<bool>(pos) = extract<bool>(value); break;
            case ISType::S8	 : obj.getAttributeValue<char>(pos) = extract<char>(value); break;
            case ISType::U8	 : obj.getAttributeValue<unsigned char>(pos) = extract<unsigned char>(value); break;
            case ISType::S16     : obj.getAttributeValue<short>(pos) = extract<short>(value); break;
            case ISType::U16     : obj.getAttributeValue<unsigned short>(pos) = extract<unsigned short>(value); break;
            case ISType::S32     : obj.getAttributeValue<int>(pos) = extract<int>(value); break;
            case ISType::U32     : obj.getAttributeValue<unsigned int>(pos) = extract<unsigned int>(value); break;
            case ISType::S64     : obj.getAttributeValue<int64_t>(pos) = extract<int64_t>(value); break;
            case ISType::U64     : obj.getAttributeValue<uint64_t>(pos) = extract<uint64_t>(value); break;
            case ISType::Float   : obj.getAttributeValue<float>(pos) = extract<float>(value); break;
            case ISType::Double  : obj.getAttributeValue<double>(pos) = extract<double>(value); break;
            case ISType::String  : obj.getAttributeValue<std::string>(pos) = extract<std::string>(value); break;
            case ISType::Date    : obj.getAttributeValue<OWLDate>(pos) = extract<OWLDate>(value); break;
            case ISType::Time    : obj.getAttributeValue<OWLTime>(pos) = extract<OWLTime>(value); break;
            case ISType::InfoObject  : break;
            default:
                assert(false);
                break;
            }
        }
    }

    void setAttributeValue_by_name(ISInfoDynAny& obj, const std::string& name, object value)
    {
        setAttributeValue_by_position(obj, obj.getDescription().attributePosition(name), value);
    }


    //
    // Work arounds
    //
    void (ISInfoDictionary::*dict_checkin)(const std::string&, const ISInfo&, bool) const = &ISInfoDictionary::checkin;
    void (ISInfoDictionary::*dict_update)(const std::string&, const ISInfo&, bool) const = &ISInfoDictionary::update;

    BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(checkin_overload, checkin, 0, 1)
    BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(dict_checkin_overload, checkin, 2, 3)
    BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(dict_update_overload, update, 2, 3)

    // BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(checkout_overload, checkout, 0, 1)


    object InfoIterator_tags(ISInfoIterator& self)
    {
        using namespace std;

        vector<pair<int,OWLTime> > tag_values;
        self.tags(tag_values);

        dict result;
        for(vector<pair<int,OWLTime> >::iterator it = tag_values.begin();
            it != tag_values.end();
            ++it) {
            result[(*it).first] = object((*it).second);
        }
        return result;
    }

    object InfoDictionary_getTags(ISInfoDictionary& self, const std::string& name)
    {
        std::vector<std::pair<int, OWLTime> > tags;
        self.getTags(name,tags);
        list result;
        for(std::vector<std::pair<int, OWLTime> >::const_iterator it = tags.begin();
            it != tags.end();
            ++it) {
            result.append(make_tuple((*it).first, (*it).second));
        }
        return result;
    }

    void InfoStream_get(ISInfoStream& self, ISInfoDynAny& result)
    {
        self >> result;
    }

    dict Info_annotations(ISInfo& self)
    {
        dict result;
        for (auto& pair: self.annotations()) {
            result[pair.first] = pair.second.data();
        }
        return result;
    }

    void resize_helper(std::vector<ISInfoDynAny>& v, size_t n)
    {
        v.resize(n);
    }

    std::string getFormat(const ISInfoDocument::Attribute& self)
    {
        auto fmt = self.format();
        if(fmt == std::hex) return "0x{:x}";
        if(fmt == std::oct) return "0o{:o}";
        return "{}";
    }

}

BOOST_PYTHON_MODULE(libispy)
{

    class_<ISType>("ISType")
        .def("name", &ISType::name, return_value_policy<copy_const_reference>())
        .def("entries", &ISType::entries)
        .def("entryType", &ISType::entryType)
        .def("entryTypeName", &ISType::entryTypeName, return_value_policy<copy_const_reference>())
        .def("entryArray", &ISType::entryArray)
        ;

    enum_<ISCriteria::Logic>("Logic")
        .value("AND", ISCriteria::AND)
        .value("OR", ISCriteria::OR)
        ;

    class_<ISCriteria>("ISCriteria", init<std::string>())
        .def(init<ISType>())
        .def(init<std::string,ISType,ISCriteria::Logic>())
        ;

    enum_<ISType::Basic>("Basic")
        .value("Error", ISType::Error)
        .value("Boolean", ISType::Boolean)
        .value("S8", ISType::S8)
        .value("U8", ISType::U8)
        .value("S16", ISType::S16)
        .value("U16", ISType::U16)
        .value("S32", ISType::S32)
        .value("U32", ISType::U32)
        .value("S64", ISType::S64)
        .value("U64", ISType::U64)
        .value("Float", ISType::Float)
        .value("Double", ISType::Double)
        .value("Date", ISType::Date)
        .value("Time", ISType::Time)
        .value("String", ISType::String)
        .value("ISInfo", ISType::InfoObject)
        ;

    class_<ISInfo, boost::noncopyable>("ISInfo", no_init)
        .def("type", &ISInfo::type, return_internal_reference<>())
        .def("time", &ISInfo::time, return_value_policy<copy_const_reference>())
        .def("tag", &ISInfo::tag)
        .def("sendCommand", &ISInfo::sendCommand)
        .def("providerExist", &ISInfo::providerExist)
        .def("provider", &ISInfo::provider)
        .def("annotations", &Info_annotations)
        ;

    class_<ISInfoAny, bases<ISInfo>, boost::noncopyable>("ISInfoAny")
        .def("reset", &ISInfoAny::reset)
        .def("countAttributes", &ISInfoAny::countAttributes)
        .def("getAttributeType",&ISInfoAny::getAttributeType)
        .def("getArraySize", &ISInfoAny::getArraySize)
        .def("readArraySize", &ISInfoAny::readArraySize)
        .def("isAttributeArray", &ISInfoAny::isAttributeArray)
        .def("get", &any_get)
        ;


    class_<ISServerIterator, boost::noncopyable>("ISServerIterator", init<IPCPartition>())
        .def("next", &ISServerIterator::operator ())
        .def("prev", (bool (ISServerIterator::*)())&ISServerIterator::operator--)
        .def("reset", &ISServerIterator::reset)
        .def("entries", &ISServerIterator::entries)
        .def("name", &ISServerIterator::name)
        .def("owner", &ISServerIterator::owner)
        .def("host", &ISServerIterator::host)
        .def("pid", &ISServerIterator::pid)
        .def("time", &ISServerIterator::time)
        ;

    class_<ISInfoDictionary>("ISInfoDictionary", init<IPCPartition>())
        .def("checkin", dict_checkin, dict_checkin_overload())
        .def("checkin", (void (ISInfoDictionary::*)(const std::string&, int, const ISInfo& ) const)&ISInfoDictionary::checkin)
        .def("contains", &ISInfoDictionary::contains)
        .def("insert", (void (ISInfoDictionary::*)(const std::string& , const ISInfo& ) const)&ISInfoDictionary::insert)
        .def("insert", (void (ISInfoDictionary::*)(const std::string& , int , const ISInfo& ) const)&ISInfoDictionary::insert)
        .def("findType", &ISInfoDictionary::findType)
        .def("findValue", (void (ISInfoDictionary::*)(const std::string&, ISInfo&) const)&ISInfoDictionary::findValue)
        .def("findValue", (void (ISInfoDictionary::*)(const std::string&, int, ISInfo&) const)&ISInfoDictionary::findValue)
        .def("getType", &ISInfoDictionary::getType)
        .def("getValue", (void (ISInfoDictionary::*)(const std::string&, ISInfo&) const)&ISInfoDictionary::getValue)
        .def("getValue", (void (ISInfoDictionary::*)(const std::string&, int tag, ISInfo&) const)&ISInfoDictionary::getValue)
        .def("getTags", &InfoDictionary_getTags)
        .def("remove", (void (ISInfoDictionary::*)(const std::string&) const)&ISInfoDictionary::remove)
        .def("update", dict_update, dict_update_overload())
        .def("update", (void (ISInfoDictionary::*)(const std::string&, int, const ISInfo&) const)&ISInfoDictionary::update)
        .def("getValues", &ISInfoDictionary::getValues<ISInfoDynAny>)
        ;

    class_<ISInfoStream, boost::noncopyable>("ISInfoStream", init<IPCPartition, std::string, ISCriteria, bool, int>())
        .def("eof", &ISInfoStream::eof)
        // .def("historyDepth", &ISInfoStream::historyDepth)
        .def("skip", &ISInfoStream::skip)
        .def("partition", &ISInfoStream::partition, return_value_policy<copy_const_reference>())
        .def("name", &ISInfoStream::name, return_value_policy<copy_const_reference>())
        .def("type", &ISInfoStream::type)
        .def("get", InfoStream_get)
        ;

    class_<ISInfoIterator, bases<ISInfoStream>, boost::noncopyable>("ISInfoIterator", init<IPCPartition, std::string, ISCriteria>())
        .def("reset", &ISInfoIterator::reset)
        .def("entries", &ISInfoIterator::entries)
        //	.def("name", &ISInfoIterator::name)
        // .def("partition",&ISInfoIterator::partition, return_value_policy<copy_const_reference>())
        .def("next", (bool (ISInfoIterator::*)())&ISInfoIterator::operator++)
        .def("prev", (bool (ISInfoIterator::*)())&ISInfoIterator::operator--)
        .def("more", &ISInfoIterator::operator bool)
        .def("type", &ISInfoIterator::type)
        .def("value", (void (ISInfoIterator::*)(ISInfo& ) const)&ISInfoIterator::value)
        .def("value", (void (ISInfoIterator::*)(int, ISInfo& ) const)&ISInfoIterator::value)
        .def("tag", &ISInfoIterator::tag)
        .def("tags", &InfoIterator_tags)
        .def("time", &ISInfoIterator::time)
        .def("sendCommand", &ISInfoIterator::sendCommand)
        ;


    class_<ISInfoReceiver, boost::noncopyable>("ISInfoReceiver", init<IPCPartition, optional<bool> >())
        .def("subscribe_info", subscribe_1<ISCallbackInfo>)
        .def("subscribe_info", subscribe_2<ISCallbackInfo>)
        .def("subscribe_info", subscribe_3<ISCallbackInfo>)
        .def("subscribe_info", subscribe_4<ISCallbackInfo>)
        .def("subscribe_event", subscribe_1<ISCallbackEvent>)
        .def("subscribe_event", subscribe_2<ISCallbackEvent>)
        .def("subscribe_event", subscribe_3<ISCallbackEvent>)
        .def("subscribe_event", subscribe_4<ISCallbackEvent>)
        .def("scheduleSubscription_info", scheduleSubscription_1<ISCallbackInfo>)
        .def("scheduleSubscription_info", scheduleSubscription_2<ISCallbackInfo>)
        .def("scheduleSubscription_info", scheduleSubscription_3<ISCallbackInfo>)
        .def("scheduleSubscription_info", scheduleSubscription_4<ISCallbackInfo>)
        .def("scheduleSubscription_event", scheduleSubscription_1<ISCallbackEvent>)
        .def("scheduleSubscription_event", scheduleSubscription_2<ISCallbackEvent>)
        .def("scheduleSubscription_event", scheduleSubscription_3<ISCallbackEvent>)
        .def("scheduleSubscription_event", scheduleSubscription_4<ISCallbackEvent>)
        /*
          .def("subscribe_info", subscribe_info1)
          .def("subscribe_info", subscribe_info2)
          .def("subscribe_info", subscribe_info3)
          .def("subscribe_info", subscribe_info4)
          .def("subscribe_event", subscribe_event1)
          .def("subscribe_event", subscribe_event2)
          .def("subscribe_event", subscribe_event3)
          .def("subscribe_event", subscribe_event4)
        */
        .def("unsubscribe", (void (ISInfoReceiver::*)(const std::string&, bool))&ISInfoReceiver::unsubscribe)
        .def("unsubscribe", (void (ISInfoReceiver::*)(const std::string&, const ISCriteria& , bool))&ISInfoReceiver::unsubscribe)
        ;

    class_<ISInfoDocument>("ISInfoDocument", init<IPCPartition,ISType>())
        .def(init<IPCPartition, const std::string>())
        // .def(init<IPCPartition, ISInfo>())
        .def("attributeCount", &ISInfoDocument::attributeCount)
        .def("description", &ISInfoDocument::description, return_value_policy<copy_const_reference>())
        .def("name", &ISInfoDocument::name, return_value_policy<copy_const_reference>())
        .def("attribute", (const ISInfoDocument::Attribute& (ISInfoDocument::*)(size_t ) const)&ISInfoDocument::attribute, return_internal_reference<>())
        .def("attribute", (const ISInfoDocument::Attribute& (ISInfoDocument::*)(const std::string& ) const)&ISInfoDocument::attribute, return_internal_reference<>())
        .def("attributePosition", &ISInfoDocument::attributePosition)
        ;

    class_<ISInfoDocument::Attribute>("ISInfoAttribute", no_init)
        .def("name", &ISInfoDocument::Attribute::name, return_value_policy<copy_const_reference>())
        .def("description", &ISInfoDocument::Attribute::description, return_value_policy<copy_const_reference>())
        .def("range", &ISInfoDocument::Attribute::range, return_value_policy<copy_const_reference>())
        .def("typeName", &ISInfoDocument::Attribute::typeName, return_value_policy<copy_const_reference>())
        .def("typeCode", &ISInfoDocument::Attribute::typeCode)
        .def("isArray", &ISInfoDocument::Attribute::isArray)
        .def("format",  getFormat);
	;

    class_<ISInfoDynAny, bases<ISInfo>, boost::noncopyable >("ISInfoDynAny")
        .def(init<IPCPartition, std::string>())
        .def(init<IPCPartition, ISType>())
        .def("getAttributesNumber", &ISInfoDynAny::getAttributesNumber)
        .def("isAttributeArray", (bool (ISInfoDynAny::*)(size_t ) const)&ISInfoDynAny::isAttributeArray)
        .def("isAttributeArray", (bool (ISInfoDynAny::*)(const std::string& ) const)&ISInfoDynAny::isAttributeArray)
        .def("getAttributeValue", getAttributeValue_by_position)
        .def("getAttributeValue", getAttributeValue_by_name)
        .def("setAttributeValue", setAttributeValue_by_position)
        .def("setAttributeValue", setAttributeValue_by_name)
        .def("getAttributeDescription", (const ISInfoDocument::Attribute& (ISInfoDynAny::*)(size_t ) const )&ISInfoDynAny::getAttributeDescription, return_internal_reference<>())
        .def("getAttributeDescription", (const ISInfoDocument::Attribute& (ISInfoDynAny::*)(const std::string& ) const )&ISInfoDynAny::getAttributeDescription, return_internal_reference<>())
        .def("getAttributeType", (ISType::Basic (ISInfoDynAny::*)(size_t) const)&ISInfoDynAny::getAttributeType)
        .def("getAttributeType", (ISType::Basic (ISInfoDynAny::*)(const std::string& ) const)&ISInfoDynAny::getAttributeType)
        .def("getDescription", (const ISInfoDocument& (ISInfoDynAny::*)() const)&ISInfoDynAny::getDescription, return_internal_reference<>())
        ;

    enum_<ISInfo::Reason>("Reason")
        .value("Created",ISInfo::Created)
        .value("Updated",ISInfo::Updated)
        .value("Deleted",ISInfo::Deleted)
        .value("Subscribed",ISInfo::Subscribed)
        ;

    class_<ISCallbackEvent, boost::noncopyable>("ISCallbackEvent", no_init)
        .def("name", &ISCallbackEvent::name)
        .def("reason", &ISCallbackEvent::reason)
        .def("type", &ISCallbackEvent::type)
        .def("time", &ISCallbackEvent::time)
        .def("tag", &ISCallbackEvent::tag)
        ;

    class_<ISCallbackInfo,bases<ISCallbackEvent>, boost::noncopyable>("ISCallbackInfo", no_init)
        .def("value", &ISCallbackInfo::value)
        ;

    class_<std::alloc_wrapper, bases<ISInfoDynAny>, boost::noncopyable>("alloc_wrapper", no_init)
        ;

    typedef std::vector<ISInfoDynAny> V;

    class_<V, boost::noncopyable>("ISInfoDynAnyVector", no_init) // , init<IPCPartition,std::string>())
        .def("resize", resize_helper)
        .def("__len__", &V::size)
        .def("__getitem__",(V::const_reference (V::*)(size_t) const)&V::at, return_internal_reference<>())
        ;
}
