
#include "ipc/core.h"
#include "ipc/partition.h"
#include "oh/OHRootHistogram.h"
#include "oh/OHRootReceiver.h"
#include "oh/OHRootProvider.h"
#include "oh/OHSubscriber.h"

#include <boost/python.hpp>
#include <boost/shared_ptr.hpp>

#include <string>

#include "TPython.h"

using namespace boost::python;

namespace {

    PyObject *getRootObject(IPCPartition& part,
                            const std::string& server,
                            const std::string& provider,
                            const std::string& histoname,
                            int tag = -1)
    {
        OHRootObject obj = OHRootReceiver::getRootObject(part, server, provider, histoname, tag);
        TObject *result = obj.object.release();
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
        return TPython::CPPInstance_FromVoidPtr(result, result->ClassName(), kTRUE);
#else
        return TPython::ObjectProxy_FromVoidPtr(result, result->ClassName());
#endif
    }

    object getRootObjectWithInfo(IPCPartition& part,
                                 const std::string& server,
                                 const std::string& provider,
                                 const std::string& histoname,
                                 int tag = -1)
    {
        OHRootObject obj = OHRootReceiver::getRootObject(part, server, provider, histoname, tag);


        boost::python::list  ann;
        for(std::vector< std::pair< std::string, std::string> >::const_iterator it = obj.annotations.begin();
            it != obj.annotations.end();
            ++it) {
            ann.append(make_tuple((*it).first, (*it).second));
        }

        TObject *result = obj.object.release();
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
        handle<> h(TPython::CPPInstance_FromVoidPtr(result, result->ClassName(), kTRUE));
#else
        handle<> h(TPython::ObjectProxy_FromVoidPtr(result, result->ClassName()));
#endif

        return make_tuple(h,
                          obj.time,
                          obj.tag,
                          ann);
    }

    BOOST_PYTHON_FUNCTION_OVERLOADS(getRootObject_overloads, getRootObject, 4, 5)
    BOOST_PYTHON_FUNCTION_OVERLOADS(getRootObjectWithInfo_overloads, getRootObjectWithInfo, 4, 5)

    struct callback_helper {
        callback_helper(const PyGILState_STATE& state)
            : m_state(state)
        {}

        ~callback_helper()
        {
            PyGILState_Release(m_state);
        }
        PyGILState_STATE m_state;
    };

    struct OHRootReceiver_wrap : OHRootReceiver, wrapper<OHRootReceiver> {
        using OHRootReceiver::receive;

        void receive (OHRootHistogram& obj)
        {
            receive_object(obj.histogram.release());
        }

        void receive (OHRootGraph& obj)
        {
            receive_object(obj.graph.release());
        }

        void receive (OHRootGraph2D& obj)
        {
            receive_object(obj.graph.release());
        }

        virtual void receive_object(TObject* obj)
        {
            callback_helper helper(PyGILState_Ensure());

#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
            handle<> h(TPython::CPPInstance_FromVoidPtr(obj, obj->ClassName(), kTRUE));
#else
            handle<> h(TPython::ObjectProxy_FromVoidPtr(obj, obj->ClassName()));
#endif
            if(override f = this->get_override("receive")) {
                f(h);
            }
        }
    };

    void subscribe_re(OHSubscriber& sub,  const std::string & server, const std::string & provider,
                      const std::string& histoname, bool provide_values)
    {
        OWLRegexp regex(provider);
        sub.subscribe(server, regex, histoname, provide_values);
    }

    void unsubscribe_re(OHSubscriber& sub,  const std::string & server, const std::string & provider,
                        const std::string& histoname)
    {
        OWLRegexp regex(provider);
        sub.unsubscribe(server, regex, histoname);
    }

    void subscribe_re2(OHSubscriber& sub,  const std::string & server, const std::string & provider,
                       const std::string& histoname, bool provide_values)
    {
        OWLRegexp regex(provider);
        OWLRegexp regex2(histoname);
        sub.subscribe(server, regex, regex2, provide_values);
    }

    void unsubscribe_re2(OHSubscriber& sub,  const std::string & server, const std::string & provider,
                         const std::string& histoname)
    {
        OWLRegexp regex(provider);
        OWLRegexp regex2(histoname);
        sub.unsubscribe(server, regex, regex2);
    }

    void publish(OHRootProvider& provider, PyObject *h,  const std::string& name, int tag = -1,
                 list annotations = list())
    {
        std::vector<std::pair<std::string,std::string>> ann;

        for(boost::python::ssize_t i = 0; i < len(annotations); i++)  {
            object pair = annotations[i];
            std::string name  = extract<std::string>(pair[0]);
            std::string value = extract<std::string>(pair[1]);
            ann.emplace_back(name, value);
        }

#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
        if(TObject *obj = reinterpret_cast<TObject*>(TPython::CPPInstance_AsVoidPtr(h))) {
#else
        if(TObject *obj = reinterpret_cast<TObject*>(TPython::ObjectProxy_AsVoidPtr(h))) {
#endif
            if(TH1 *histo = dynamic_cast<TH1*>(obj)) {
                provider.publish(*histo, name, tag, ann);
            } else if (TGraph *graph = dynamic_cast<TGraph*>(obj)) {
                    provider.publish(*graph, name, tag, ann);
            } else if (TGraph2D *graph = dynamic_cast<TGraph2D*>(obj)) {
                provider.publish(*graph, name, tag, ann);
            } else if (TEfficiency *eff = dynamic_cast<TEfficiency*>(obj)) {
                provider.publish(*eff, name, tag, ann);
            }
        }
    }

    BOOST_PYTHON_FUNCTION_OVERLOADS(publish_overloads, publish, 3, 5)

}

BOOST_PYTHON_MODULE(libohpy)
{
    class_<OHSubscriber, boost::noncopyable>("OHSubscriber", init<IPCPartition,std::string,OHReceiver&,bool>())
        .def("subscribe", (void (OHSubscriber::*)( const std::string &, const std::string &, const std::string&, bool))&OHSubscriber::subscribe)
        .def("unsubscribe", (void (OHSubscriber::*)( const std::string &, const std::string &, const std::string&))&OHSubscriber::unsubscribe)
        .def("subscribe_re", subscribe_re)
        .def("unsubscribe_re", unsubscribe_re)
        .def("subscribe_re2", subscribe_re2)
        .def("unsubscribe_re2", unsubscribe_re2)
        ;

        class_<OHReceiver, boost::noncopyable>("OHReceiver", no_init)
            ;

        class_<OHRootReceiver_wrap, bases<OHReceiver>, boost::noncopyable>("OHRootReceiver")
            ;

        def("getRootObject", getRootObject, getRootObject_overloads())
            ;

        def("getRootObjectInfo", getRootObjectWithInfo, getRootObjectWithInfo_overloads())
            ;

        class_<OHRootProvider, boost::noncopyable>("OHRootProvider", init<IPCPartition,std::string,std::string,OHCommandListener*>())
            .def("publish", publish, publish_overloads())
            ;
}
