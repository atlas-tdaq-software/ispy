
#include "ers/Issue.h"
#include "ipc/exceptions.h"
#include "ipc/core.h"

#include "ers/IssueReceiver.h"
#include "ers/RemoteContext.h"
#include "ers/AnyIssue.h"

#include "owl/time.h"

#include <boost/python.hpp>

#include <string>
#include <chrono>

using namespace boost::python;

namespace {

    struct IPCInitializer {
        IPCInitializer()
        {
            setenv("TDAQ_IPC_CLIENT_INTERCEPTORS", "", 0);
            if(!IPCCore::isInitialised() && getenv("TDAQ_MTSPY_NOINIT") == nullptr) {
                std::list<std::pair<std::string, std::string>>  options;
                IPCCore::init(options);
            }
        }
    };

    class MTSReceiver : public ers::IssueReceiver {
    public:
        MTSReceiver(object callback)
            : m_callback(callback)
        {
        }

        ~MTSReceiver()
        {
            if(!m_removed)
                ers::StreamManager::instance().remove_receiver(this);
        }

        void remove_receiver()
        {
            if(!m_removed)
                ers::StreamManager::instance().remove_receiver(this);
            m_removed = true;
        }

        void receive(const ers::Issue& issue) override
        {
            if(m_callback) {
                auto state = PyGILState_Ensure();
                {
                    dict iss;
                    iss["msg"]     = issue.get_class_name();
                    iss["sev"]     = ers::to_string(issue.severity());
                    iss["time"]    = issue.time();
                    iss["message"] = issue.message();

                    dict params;
                    for(auto& p : issue.parameters()) {
                        params[p.first] = p.second;
                    }
                    iss["params"] = params;

                    list qual;
                    for(auto q : issue.qualifiers()) {
                        qual.append(q);
                    }
                    iss["qual"] = qual;

                    iss["app"]  = issue.context().application_name();
                    iss["host"] = issue.context().host_name();
                    iss["file"] = issue.context().file_name();
                    iss["func"] = issue.context().function_name();
                    iss["pkg"]  = issue.context().package_name();
                    iss["line"] = issue.context().line_number();
                    iss["user"] = issue.context().user_name();
                    iss["pid"] = issue.context().process_id();

                    m_callback(iss);
                }
                PyGILState_Release(state);
            }
        }
    private:
        object m_callback;
        bool   m_removed{false};
    };

    MTSReceiver* add_receiver(object callable, const std::string& partition, const std::string& subscription)
    {
        MTSReceiver *receiver = new MTSReceiver(callable);
        ers::StreamManager::instance().add_receiver("mts", { partition, subscription}, receiver);
        return receiver;
    }

    ers::AnyIssue make_issue(const ers::RemoteContext& ctx,
                             const std::string& msgId,
                             const std::string& sev_as_string,
                             const std::string& time_as_string,
                             const std::string& message,
                             list quals,
                             dict params)
    {
        ers::Severity sev{ers::Warning};
        ers::parse(sev_as_string, sev);

        OWLTime time{time_as_string.c_str()};
        auto t = std::chrono::system_clock::from_time_t(time.c_time()) + std::chrono::microseconds(time.mksec());

        std::vector<std::string> qualifiers;
        for(long i = 0; i < len(quals); i++) {
            qualifiers.push_back(extract<std::string>(quals[i]));
        }

        std::map<std::string, std::string> parameters;

        list keys = params.keys();
        for(long i = 0; i < len(keys); i++) {
            std::string key = extract<std::string>(keys[i]);
            std::string val = extract<std::string>(params[keys[i]]);
            parameters[key] = val;
        }

        return ers::AnyIssue{
            msgId,
            sev,
            ctx,
            t,
            message,
            qualifiers,
            parameters};
    }

    void send_issue(ers::AnyIssue& issue)
    {
        switch (issue.severity().type) {
        case ers::Debug:
            ers::debug(issue);
            break;
        case ers::Log:
            ers::log(issue);
            break;
        case ers::Information:
            ers::info(issue);
            break;
        case ers::Warning:
            ers::warning(issue);
            break;
        case ers::Error:
            ers::error(issue);
            break;
        case ers::Fatal:
            ers::fatal(issue);
            break;
        }
    }

}

BOOST_PYTHON_MODULE(libmtspy)
{
    static IPCInitializer ipc_initialize;

    class_<MTSReceiver, boost::noncopyable>("MTSReceiver", no_init)
        .def("remove_receiver", &MTSReceiver::remove_receiver);
    def("add_receiver", add_receiver, return_value_policy<reference_existing_object>());

    class_<ers::RemoteProcessContext>("RemoteProcessContext",
                                      init<std::string, int, int, std::string, int, std::string, std::string>());

    class_<ers::RemoteContext>("RemoteContext",
                               init<std::string, std::string, int, std::string, ers::RemoteProcessContext>());

    class_<ers::AnyIssue>("AnyIssue", no_init);

    def("make_issue", make_issue);
    def("send_issue", send_issue);
}
